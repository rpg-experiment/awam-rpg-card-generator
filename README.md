# AWAM card generator

[AWAM (Anime Was A Mistake) RPG](http://editions-6napse.fr/#product-awam) card generator.

## Usage

Go to the [page]() and generated some card for the game.

Example with gimmick

![demo_lorem-ipsumm_gimmick.png](resources/demo_lorem-ipsumm_gimmick.png)

## DEV

```
npm ci
npm run build
npm run preview
```
