export class Card {
    name: string;
    bonus: string;
    condition: string;

    constructor(name: string, bonus: string, condition: string) {
        this.name = name;
        this.bonus = bonus;
        this.condition = condition;
    }
}
