import {Card} from './Card';
import gimmickCardUrl from '../assets/gimmick.jpg'

interface ICardData {
    name: string;
    bonus: string;
    condition: string;
}

export class CardGenerator implements ICardData {
    name: string;
    bonus: string;
    condition: string;

    constructor(name: string, bonus: string, condition: string) {
        this.name = name;
        this.bonus = bonus;
        this.condition = condition;
    }

    generate() {
        const card = new Card(this.name, this.bonus, this.condition);

        const canvas = document.getElementById('canvas') as HTMLCanvasElement

        const ctx = canvas.getContext('2d');
        if (!ctx)
        {
            console.log('no context')
            return;
        }

        this.loadImage(gimmickCardUrl).then((backgroundImage) =>
        {
            ctx.drawImage(backgroundImage, 0, 0, canvas.width, canvas.height);

            ctx.font = '24px Arial';
            const lineHeight = 24 + 2; // pixel size + margin ;)
            this.drawText(card.name, ctx, 50, lineHeight, [
                {posX: 120, widthX: 400 - 120, nextY: 3},
                {posX: 12, widthX: 400 - 12},
                {posX: 12, widthX: 510 - 12},
                {posX: 12, widthX: 510 - 12},
            ]);
            this.drawText(card.bonus, ctx, 165, lineHeight, [
                {posX: 128, widthX: 510 - 128, nextY: 3},
                {posX: 12, widthX: 510 - 12},
                {posX: 12, widthX: 510 - 12},
                {posX: 12, widthX: 510 - 12}
            ]);
            this.drawText(card.condition, ctx, 272, lineHeight, [
                {posX: 182, widthX: 510 - 182, nextY: 3},
                {posX: 12, widthX: 408 - 12},
                {posX: 12, widthX: 408 - 12},
                {posX: 12, widthX: 408 - 12}
            ]);
        })
    }

    private async loadImage(src: string): Promise<HTMLImageElement> {
        const image = new Image();
        image.src = src;
        return new Promise(resolve => {
            image.onload = (ev) => {
                resolve(image);
            }
        });
    }

    private drawText(txt: string, context: CanvasRenderingContext2D, linePosY: number, lineHeightPx: number, fillers: TextFiller[]) {
        let lines = [];
        let words = txt.split(' ');
        let tmpLine = '';
        let tmpPosY = linePosY;

        // distribute word on lines
        while (words.length > 0) {
            let word = words.shift() ?? '';

            const lineWidth = context.measureText(tmpLine + word).width;

            // reach line limit, so cut to a new line

            if (lineWidth > fillers[lines.length].widthX) {
                lines.push(tmpLine.trimEnd());
                tmpLine = '';

                // reach limit of lines, skip it (lines.length grow each time)
                if (fillers.length <= lines.length) {
                    // add ending on current line
                    lines[lines.length - 1] += '...';
                    break;
                }
            }

            tmpLine += word + ' ';
        }

        // add last line when not empty
        if (tmpLine.length > 0) {
            lines.push(tmpLine);
        }

        // write each lines
        lines.forEach((line, lineNumber) => {
            context.fillText(line, fillers[ lineNumber ].posX, tmpPosY);
            tmpPosY += lineHeightPx + (fillers[ lineNumber ].nextY ?? 0);
        });
    }
}

type TextFiller = {
    // start to write text
    posX: number,
    // end x (posX+widthX
    widthX: number,
    nextY?: number
}

